### What is this repository for? ###

This is the linux on android repo. This is where i will keep the scripts that i use to help me intagrate linux into android without tainting android in a negative way.

Version: 1.0

This is not meant to be used on all phones. This was devolped on a Oukitel K10000.

### What do the files do? ###

mount.sh - mounts the sdcard and preps the root directory

file - runs the programs stored on the sdcard

file.old - Old version of file before a rewrite

setup.sh - integrates linux into android much more by starting services, finish populating /dev, etc

## What is this licensed under? ###

This is licensed under the GNU GPLv2
See license.txt for more info