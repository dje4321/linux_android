export ROOT=/target

setenforce 0

mount -o remount,rw /

mkdir /tmp; mount -t tmpfs tmpfs /tmp

mkdir /target
mount -t ext4 -o exec,suid,dev /dev/block/mmcblk1p1 $ROOT #Change /dev/XXX to be the whatever you sdcard is
for f in dev dev/pts proc sys ; do mount -o bind /$f $ROOT/$f ; done

ln -s $ROOT/bin /bin
rm /etc; ln -s $ROOT/etc /etc
ln -s $ROOT/lib /lib
ln -s $ROOT/home /home
ln -s $ROOT/opt /opt
rm -r /root; ln -s $ROOT/root /root
ln -s $ROOT/run /run
ln -s $ROOT/usr /usr
ln -s $ROOT/var /var

cp -rf /sbin/* $ROOT/sbin/; rm -r /sbin; ln -s $ROOT/sbin /sbin
			
mount -o remount,ro /
